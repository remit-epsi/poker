# Poker

Notre projet devra


- [x] être versionné sur GIT (plateforme de votre choix)

- [x] avoir un fichier README avec la description du projet et des fonctionnalités mises en place

- [x] chaque membre de votre équipe doit avoir commité

- [x] avoir un fichier requirements.txt

## Cloner notre projet

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:120ec6f682dc8f3644ba0dc6b6e61fb7?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:120ec6f682dc8f3644ba0dc6b6e61fb7?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:120ec6f682dc8f3644ba0dc6b6e61fb7?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/remit-epsi/poker.git
git branch -M main
git push -uf origin main
```

## Obligatoire


- Un joueur jouant face à 3 IA.

- Génération des cartes et distribution aléatoire (2 cartes par joueur, 3 cartes sur la table)

- Avoir une représentation graphique du jeu. (TK ou autre)

- Il doit être possible de saisir un seed (graine), ce qui génèrera la même répartition des cartes.

## Déroulé du jeu

Pour lancer le jeu, il vous sufft de lancer le main.py qui appelle le app.py.

Vous verrez s'afficher 3 bots avec des cartes retournées et vous-même le joueur 1.

Vous pouvez ensuite miser 1 ou 2 € et quand votre pot arrive à 0, les cartes sont révélées et le vainqueur est annoncé avec sa combinaison.

Le seed de la partie se trouve dans l'onglet SEED situé en haut à gauche.

Bonne partie.

## Béta

Dans la béta, le app.py servait de menu d'accueil pour lancer le jeu avec un bouton play mais il s'est avéré compliqué de lier les deux fenêtres Tkinter.

Le choix a été fait de tout insérer dans le app.py.

## Test

Le fichier main_test.py permet d'effectuer différents tests sur la génération de cartes, la vérification de la main et la vérification du deck.

## Seed 
Pour retrouver les différentes combinaisons au premier tour, vous pouvez entrer ces seeds dans le jeu :

-  Paire : 257310117

- Double paire : 800377140

- Brelan : 63069946

- Quinte : 564055240

- Couleur : 379119659

- Full House : 511532271

- Carré : 113397571

- Quinte flush : 855053106

- Quinte flush royale : 554843405


