class Card:
    """Classe Card qui permet d'instancier une carte"""
    def __init__(self, value, name, categorie, img_source):
        """Instancie une carte avec sa valeur, son nom, sa catégorie et la source de l'image"""
        self.__value = value
        self.__name = name
        self.__categorie = categorie
        self.__img_source = img_source

    def __str__(self):
        """Retourne le nom et la couleur de la carte"""
        return self.__name + ' de ' + self.__categorie

    def compare(self, card):
        """Compare la carte actuelle avec la carte passée en paramètre"""
        if card.value > self.__value:
            return card.__str__() + ' est plus fort que ' + self.__str__()
        elif card.value < self.__value:
            return self.__str__() + ' est plus fort que ' + card.__str__()
        else:
            return self.__str__() + ' et ' + card.__str__() + ' sont égaux.'

    def print_card(self):
        """Affiche les informations de la carte"""
        return str(self.__name) + " de " + str(self.categorie)

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, value):
        self.__value = value

    @property
    def img_source(self):
        return self.__img_source

    @img_source.setter
    def img_source(self, img_source):
        self.__img_source = img_source

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

    @property
    def categorie(self):
        return self.__categorie

    @categorie.setter
    def categorie(self, categorie):
        self.__categorie = categorie

    pass