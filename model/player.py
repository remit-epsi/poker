class Player:
    """Classe Player permettant d'initialiser un nouveau joueur"""
    def __init__(self, name, hand_list, amount, is_dealer):
        """Initialise un nouveau joueur selon son nom, sa main, sa cagnotte et si c'est à son tour de distribuer"""
        self.__name = name
        self.__hand = hand_list
        self.__amount = amount
        self.__is_dealer = is_dealer

    def print_hand(self):
        """Affiche les informations de sa main"""
        result = "La main de " + str(self.__name) + " est : \r\n"
        for card in self.__hand:
            result += card.print_card() + "\r\n"
        print(result)

    @property
    def is_dealer(self):
        return self.__is_dealer

    @is_dealer.setter
    def is_dealer(self, is_dealer):
        self.__is_dealer = is_dealer

    @property
    def hand(self):
        return self.__hand

    @hand.setter
    def hand(self, hand):
        self.__hand = hand

    @property
    def amount(self):
        return self.__amount

    @amount.setter
    def amount(self, amount):
        self.__amount = amount

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name
    pass