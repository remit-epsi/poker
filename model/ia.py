class Ia:
    """Classe IA permettant d'initialiser un nouveau Bot"""
    def __init__(self, name, hand_list, difficulty, is_dealer, amount):
        """Initialise un nouveau Bot selon son nom, sa main, sa difficulté et si c'est à son tour de distribuer"""
        self.__name = name
        self.__hand = hand_list
        self.__difficulty = difficulty
        self.__is_dealer = is_dealer
        self.__amount = amount

    def print_hand(self):
        """Affiche les informations de sa main"""
        result = "La main de " + str(self.__name) + " est : \r\n"
        for card in self.__hand:
            result += card.print_card() + "\r\n"
        print(result)


    @property
    def hand(self):
        return self.__hand

    @hand.setter
    def hand(self, hand):
        self.__hand = hand

    @property
    def is_dealer(self):
        return self.__is_dealer

    @is_dealer.setter
    def is_dealer(self, is_dealer):
        self.__is_dealer = is_dealer

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

    @property
    def difficulty(self):
        return self.__difficulty

    @difficulty.setter
    def difficulty(self, difficulty):
        self.__difficulty = difficulty

    @property
    def amount(self):
        return self.__amount

    @amount.setter
    def amount(self, amount):
        self.__amount = amount

    pass