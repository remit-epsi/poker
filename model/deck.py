from .card import Card

#Contient le nom et la valeur respective de la carte
VALUE_CARDS = {'As': 13, 'Deux': 1, 'Trois': 2, 'Quatre': 3, 'Cinq': 4, 'Six': 5, 'Sept': 6, 'Huit': 7, 'Neuf': 8, \
               'Dix': 9, 'Valet': 10, 'Dame': 11, 'Roi': 12}
# Liste contenant le nom du fichier image des cartes de carreau respectivement
LIST_IMG_DIAMONDS = ["as_carreau.png", "deux_carreau.png", "trois_carreau.png", "quatre_carreau.png", "cinq_carreau.png", "six_carreau.png",\
                   "sept_carreau.png", "huit_carreau.png", "neuf_carreau.png", "dix_carreau.png", "valet_carreau.png","dame_carreau.png", "roi_carreau.png"]

#Liste contenant le nom du fichier image des cartes de coeur respectivement
LIST_IMG_HEARTS = ["as_coeur.png", "deux_coeur.png", "trois_coeur.png", "quatre_coeur.png", "cinq_coeur.png", "six_coeur.png",\
                   "sept_coeur.png", "huit_coeur.png", "neuf_coeur.png", "dix_coeur.png", "valet_coeur.png","dame_coeur.png", "roi_coeur.png"]

#Liste contenant le nom du fichier image des cartes de pique respectivement
LIST_IMG_SPADES = ["as_pique.png", "deux_pique.png", "trois_pique.png", "quatre_pique.png", "cinq_pique.png", "six_pique.png",\
                   "sept_pique.png", "huit_pique.png", "neuf_pique.png", "dix_pique.png", "valet_pique.png","dame_pique.png", "roi_pique.png"]
#Liste contenant le nom du fichier image des cartes de trèfle respectivement
LIST_IMG_CLUBS = ["as_trefle.png", "deux_trefle.png", "trois_trefle.png", "quatre_trefle.png", "cinq_trefle.png", "six_trefle.png",\
                   "sept_trefle.png", "huit_trefle.png", "neuf_trefle.png", "dix_trefle.png", "valet_trefle.png","dame_trefle.png", "roi_trefle.png"]

class Deck:
    """Class deck qui contient le jeu de 52 cartes"""
    def __init__(self):
        """Initisalise un nouveau jeu de 52 cartes"""
        self.__list_card_hearts = self.init_cards("Coeur", LIST_IMG_HEARTS)
        self.__list_card_spades = self.init_cards("Pique", LIST_IMG_SPADES)
        self.__list_card_diamonds = self.init_cards("Carreau", LIST_IMG_DIAMONDS)
        self.__list_card_clubs = self.init_cards("Trefle", LIST_IMG_CLUBS)
        self.__deck = self.__list_card_clubs + self.__list_card_spades + self.__list_card_hearts + self.__list_card_diamonds

    def init_cards(self, categorie_name, list_img):
        """Initialise les cartes suivant la couleur de la carte et sa source d'image"""
        list_card = []
        i = 0
        for key, value in VALUE_CARDS.items():
            list_card.append(Card(value, key, categorie_name, list_img[i - 1]))
            i = i + 1
        return list_card

    def get_number_cards_from_deck(self, number_cards):
        """Renvoie le nombre de cartes passé en paramètre en les retirant de la pioche"""
        """list_card_to_remove = []
        for i in range(number_cards):
            card_to_remove = numpy.random.choice(self.deck, 1)
            list_card_to_remove.append(card_to_remove[0])
            del self.deck[self.deck.index(card_to_remove[0])]
        #self.deck = [card for card in self.deck if card not in list_card_to_remove]
        return list_card_to_remove"""
        list_card_to_remove = []
        for i in range(number_cards):
            list_card_to_remove.append(self.deck[len(self.deck) - 1])
            self.deck.pop()
        # self.deck = [card for card in self.deck if card not in list_card_to_remove]
        return list_card_to_remove

    def length_deck(self):
        """Renvoie le nombre de carte contenue dans la pioche"""
        return len(self.__deck)

    def print_deck(self):
        """Affiche les informations de la pioche"""
        result = "Le deck est : \r\n"
        for card in self.__deck:
            result += card.print_card() + "\r\n"
        return result

    @property
    def list_card_hearts(self):
        return self.__list_card_hearts

    @list_card_hearts.setter
    def list_card_hearts(self, list_card_hearts):
        self.__list_card_hearts = list_card_hearts

    @property
    def list_card_spades(self):
        return self.__list_card_spades

    @list_card_spades.setter
    def list_card_spades(self, list_card_spades):
        self.__list_card_spades = list_card_spades

    @property
    def list_card_diamonds(self):
        return self.__list_card_diamonds

    @list_card_diamonds.setter
    def list_card_diamonds(self, list_card_diamonds):
        self.__list_card_diamonds = list_card_diamonds

    @property
    def list_card_clubs(self):
        return self.__list_card_clubs

    @list_card_clubs.setter
    def list_card_clubs(self, list_card_clubs):
        self.__list_card_clubs = list_card_clubs

    @property
    def deck(self):
        return self.__deck

    @deck.setter
    def deck(self, deck):
        self.__deck = deck

    pass
