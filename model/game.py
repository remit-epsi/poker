import numpy
from model.ia import Ia
from model.deck import Deck
from model.player import Player

CONVERT_SEED_TO_INT = 0.000000001

NUMBER_CARD_PLAYER = 2 #Nombre de carte par joueur
BIG_BLIND = 2 #Montant de la grosse blind
SMALL_BLIND = 1 #Montant de la petite blind

class Game:
    """Classe Game qui permet d'instancier une partie"""
    def __init__(self, ia_numbers, ia_difficulty, player_numbers, seed, bankroll):
        """Initialise une partie avec le nombre d'ordinateur, leur difficultée, le nombre de joueur,
        la seed de la partie et la cagnotte de départ des joueurs
        """
        number_total_player = player_numbers + ia_numbers
        self.__player_numbers = player_numbers
        self.__ia_numbers = ia_numbers
        self.__seed = seed
        self.__ia_difficulty = ia_difficulty
        self.__deck = Deck()
        self.__bankroll = bankroll
        self.__list_player = []
        if seed == "":
            generate_seed = numpy.random.random()
            self.__seed = int(generate_seed / CONVERT_SEED_TO_INT)
        else:
            self.__seed = seed
        numpy.random.seed(self.__seed)
        numpy.random.shuffle(self.__deck.deck)
        self.__dealer = numpy.random.randint(0, number_total_player)
        is_dealer = False
        for player in range(number_total_player):
            is_dealer = True if self.dealer == player else False
            if player < ia_numbers:
                self.__list_player.append(
                    Ia("Bot " + str(player + 1), self.__deck.get_number_cards_from_deck(NUMBER_CARD_PLAYER),
                       self.__ia_difficulty, is_dealer, self.__bankroll))
            else:
                self.__list_player.append(
                    Player("Joueur " + str(player - ia_numbers + 1), self.__deck.get_number_cards_from_deck(NUMBER_CARD_PLAYER),
                           self.__bankroll, is_dealer))
        self.__draw_card = self.__deck.get_number_cards_from_deck(3)
        self.__pot = 0

    def is_color(self,list_card_player) -> int:
        """Teste si une main contient cinq cartes de même couleur"""
        list_hand = [*list_card_player, *self.__draw_card]
        if list_hand[0].categorie == list_hand[1].categorie and list_hand[0].categorie == list_hand[2].categorie and list_hand[0].categorie == list_hand[3].categorie and list_hand[0].categorie == list_hand[4].categorie:
            list_value_of_card = []
            for card in list_hand:
                list_value_of_card.append(card.value)
            return max(list_value_of_card)
        else:
            return 0

    def is_four_of_a_kind(self, list_card_player) -> int:
        """Teste si une main contient 4 cartes de même valeur et une carte autre carte"""
        list_hand = [*list_card_player, *self.__draw_card]
        return self.same_value_of_card(list_hand, 4, 1)

    def is_brelan(self, list_card_player) -> int:
        """Teste si une main contient 3 cartes de même valeur et deux autres cartes"""
        list_hand = [*list_card_player, *self.__draw_card]
        return self.same_value_of_card(list_hand, 3, 1)

    def is_pair(self,list_card_player) -> int:
        """Teste si une main contient 2 cartes de même valeur et 3 autres cartes"""
        list_hand = [*list_card_player, *self.__draw_card]
        return self.same_value_of_card(list_hand, 2, 1)

    def is_full_house(self, list_card_player) -> int:
        """Teste si une main contient une paire et un brelan"""
        list_hand = [*list_card_player, *self.__draw_card]
        if self.same_value_of_card(list_hand, 2, 1) > 0 and self.same_value_of_card(list_hand, 3, 1) > 0:
            return self.same_value_of_card(list_hand, 3, 1)
        else:
            return 0

    def is_straight_flush(self, list_card_player) -> int:
        """Teste si une main contient une 5 cartes consécutives de même couleur"""
        list_hand = [*list_card_player, *self.__draw_card]
        if self.is_color(list_card_player) > 0 and self.consecutive_value(list_hand) > 0:
            return self.consecutive_value(list_hand)
        else:
            return 0

    def is_straight(self, list_card_player) -> int:
        """Teste si une main contient 5 cartes consécutives"""
        list_hand = [*list_card_player, *self.__draw_card]
        list_value_card = []
        for card in list_hand:
            list_value_card.append(card.value)
        if self.consecutive_value(list_hand) > 0:
            return self.consecutive_value(list_hand)
        else:
            return 0

    def is_double_pair(self,list_card_player) -> int:
        """Teste si une main contient 2 paires de même valeur et 1 autre carte"""
        list_hand = [*list_card_player, *self.__draw_card]
        return self.same_value_of_card(list_hand, 2, 2)

    def is_straight_flush_royale(self, list_card_player) -> int:
        """Teste si une main contient un 10, Valet, Dame, Roi et As de la même couleur
         Donne la valeur 166 qui correspond à la plus grande main possible
        """
        list_hand = [*list_card_player, *self.__draw_card]
        list_value_card = []
        for card in list_hand:
            list_value_card.append(card.value)
        if self.is_color(list_hand) and 9 in list_value_card and 10 in list_value_card and 11 in list_value_card and 12 in list_value_card and 13 in list_value_card:
            return 166
        else:
            return False

    def height(self, list_card_player) -> int:
        """Renvoie la valeur de la plus haute carte de la main du joueur"""
        if list_card_player[0].value > list_card_player[1].value:
            return list_card_player[0].value
        else:
            return list_card_player[1].value

    def same_value_of_card(self,list_hand, number_of_card, number_of_pair) -> int:
        """Vérifie si la main contient la même valeur du nombre de carte passé en paramètre et vérifie si le nombre de pair recherché est égal au nombre de pair dans la main"""
        list_value_card = []
        result = 0
        number_of_pair_in_list = 0
        for card in list_hand:
            list_value_card.append(card.value)
        for i in range(0, 13):
            if list_value_card.count(i) == number_of_card:
                result = i
                number_of_pair_in_list = number_of_pair_in_list + 1
        if number_of_pair > 1:
            if number_of_pair_in_list == number_of_pair:
                return result
            else:
                return 0
        else:
            return result

    def consecutive_value(self, list_hand) -> int:
        """Vérifie si la main contient 5 cartes où leur valeur se suivent (ex : As, 2, 3, 4 et 5 OU 10, Valet, Dame, Roi et As"""
        list_value_card = []
        for card in list_hand:
            list_value_card.append(card.value)
        list_value_card = sorted(list_value_card)
        result = list_value_card[4]
        if 1 in list_value_card and 2 in list_value_card and 3 in list_value_card and 4 in list_value_card and 13 in list_value_card:
            result = 4
        elif 9 in list_value_card and 10 in list_value_card and 11 in list_value_card and 12 in list_value_card and 13 in list_value_card:
            result = 13
        else:
            for i in range(len(list_value_card) - 1):
                if list_value_card[i + 1] != list_value_card[i] + 1:
                    result = 0
        return result

    def next_round(self):
        """Passe au tour suivant en donnant le dealer au joueur à la gauche du dealer du tour fini et distribue les cartes"""
        del self.__deck
        del self.__draw_card
        self.__deck = Deck()
        numpy.random.shuffle(self.__deck.deck)
        is_dealer = self.dealer
        for player in self.list_player:
            player.hand = self.__deck.get_number_cards_from_deck(NUMBER_CARD_PLAYER)
            if player.is_dealer:
                self.dealer = 0 if self.list_player.index(player) + 1 >= len(self.list_player) else self.list_player.index(player) + 1
                player.is_dealer = False
        self.list_player[self.dealer].is_dealer = True
        self.__draw_card = self.__deck.get_number_cards_from_deck(3)

    def add_in_pot(self, player, amount):
        """Ajoute le montant passé en paramètre au pot du jeu"""
        self.pot = self.pot + amount
        player.amount = player.amount - amount

    def bet_small_blind(self, player):
        """Ajoute le montant de la petite blind au pot et enlève ce montant au joueur qui a parier"""
        self.pot = self.pot + SMALL_BLIND
        player.amount = player.amount - SMALL_BLIND

    def bet_big_blind(self, player):
        """Ajoute le montant de la grosse blind au pot et enlève ce montant au joueur qui a parier"""
        self.pot = self.pot + BIG_BLIND
        player.amount = player.amount - BIG_BLIND

    def check_value_win(self) -> list:
        """Vérifie quel joueur a la plus grosse main
        Hauteur : 1 -> 13 , Paire : 14 -> 27, Double pair : 28 -> 41, Brelan : 42 -> 55, Quinte : 56 -> 69, Couleur : 70 -> 83, Full house : 84 -> 97, Carré : 98 -> 111
            Quinte flush : 112 -> 152, Quinte flush royale : 166"""
        list_win = []
        for player in self.list_player:
            if self.is_straight_flush_royale(player.hand) > 0:
                list_win.append(self.is_straight_flush_royale(player.hand))
            elif self.is_straight_flush(player.hand) > 0:
                list_win.append(112 + self.is_straight_flush(player.hand))
            elif self.is_four_of_a_kind(player.hand) > 0:
                list_win.append(98 + self.is_four_of_a_kind(player.hand))
            elif self.is_full_house(player.hand) > 0:
                list_win.append(84 + self.is_full_house(player.hand))
            elif self.is_color(player.hand) > 0:
                list_win.append(70 + self.is_color(player.hand))
            elif self.is_straight(player.hand) > 0:
                list_win.append(56 + self.is_straight(player.hand))
            elif self.is_brelan(player.hand) > 0:
                list_win.append(42 + self.is_brelan(player.hand))
            elif self.is_double_pair(player.hand) > 0:
                list_win.append(28 + self.is_double_pair(player.hand))
            elif self.is_pair(player.hand) > 0:
                list_win.append(14 + self.is_pair(player.hand))
            else:
                list_win.append(self.height(player.hand))
        return list_win

    def check_winner(self):
        """Retourne le joueur qui gagne le pot et initialise son pot"""
        list_win = self.check_value_win()
        winner = max(list_win)
        self.recover_pot(self.list_player[list_win.index(winner)])
        if list_win.count(winner) > 1:
            return self.list_player[list_win.index(winner)]
        else:
            return self.list_player[list_win.index(winner)]

    def recover_pot(self, player):
        player.amount = player.amount + self.pot
        self.pot = 0

    @property
    def draw_card(self):
        return self.__draw_card

    @draw_card.setter
    def draw_card(self, draw_card):
        self.__draw_card = draw_card

    @property
    def seed(self):
        return self.__seed

    @seed.setter
    def seed(self, seed):
        self.__seed = seed

    @property
    def dealer(self):
        return self.__dealer

    @dealer.setter
    def dealer(self, dealer):
        self.__dealer = dealer

    @property
    def list_player(self):
        return self.__list_player

    @list_player.setter
    def list_player(self, list_player):
        self.__list_player = list_player

    @property
    def pot(self):
        return self.__pot

    @pot.setter
    def pot(self, pot):
        self.__pot = pot

    pass
