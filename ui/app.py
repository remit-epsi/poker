from model.game import Game
from tkinter import *
from PIL import ImageTk, Image
import tkinter as tk
import tkinter.font as tkFont
from tkinter import messagebox
from tkinter.simpledialog import askstring

HEIGHT_WINDOWS = 500 #Hauteur de la fenetre
WIDTH_WINDOWS = 600 #Largeur de la fenetre
NUMBER_OF_BOT = 3 #Nombre d'ordinateur
DIFFICULTY_OF_BOT = 1 #Difficultée des ordinateurs
NUMBER_OF_PLAYER = 1 #Nombre de joueur
NUMBER_OF_AMOUNT = 10 #Montant que les joueurs possèdent

class App:
    """Class App qui permet l'affichage du plateau et du jeu en général"""
    def __init__(self, root, seed):
        """Contructeur de la classe App"""
        self.__game = Game(NUMBER_OF_BOT, DIFFICULTY_OF_BOT, NUMBER_OF_PLAYER,  seed, NUMBER_OF_AMOUNT)
        root.title("Poker Texas Hold'em")
        root.geometry(str(WIDTH_WINDOWS) + "x" + str(HEIGHT_WINDOWS))
        root.resizable(width=False, height=False)
        self.__root = root
        self.__menubar = self.initialize_menu_bar()
        self.__seed = seed
        self.__list_amount_bot = []
        self.__bot_one_label = self.createLabel("Bot 1", 30, 170)
        self.__hand_card_one_bot_one = self.createCard("cartes.jpg", 0, 200)
        self.__hand_card_two_bot_one = self.createCard("cartes.jpg", 70, 200)
        self.__bot_one_amount_label = self.createLabel(str(self.game.list_player[0].amount) + " €", 30, 280)
        self.__list_amount_bot.append(self.__bot_one_amount_label)

        self.__bot_two_label = self.createLabel("Bot 2", 285,10)
        self.__hand_card_one_bot_two = self.createCard("cartes.jpg", 250, 40)
        self.__hand_card_two_bot_two = self.createCard("cartes.jpg", 320, 40)
        self.__bot_two_amount_label = self.createLabel(str(self.game.list_player[1].amount) + " €", 285, 120)
        self.__list_amount_bot.append(self.__bot_two_amount_label)

        self.__bot_three_label = self.createLabel("Bot 3", 495, 170)
        self.__hand_card_one_bot_three = self.createCard("cartes.jpg", 460, 200)
        self.__hand_card_two_bot_three = self.createCard("cartes.jpg", 530, 200)
        self.__bot_three_amount_label = self.createLabel(str(self.game.list_player[2].amount) + " €", 495, 280)
        self.__list_amount_bot.append(self.__bot_three_amount_label)

        self.__player_one_label = self.createLabel("Joueur 1", 285, 370)
        self.__hand_card_one_player_one = self.createCard(self.game.list_player[3].hand[0].img_source, 250, 400)
        self.__hand_card_two_player_one = self.createCard(self.game.list_player[3].hand[1].img_source, 320, 400)
        self.__player_one_amount_label = self.createLabel(str(self.game.list_player[3].amount) + " €", 285, 480)

        self.__first_card_draw = self.createCard(self.game.draw_card[0].img_source, 210, 200)
        self.__two_card_draw = self.createCard(self.game.draw_card[1].img_source, 280, 200)
        self.__three_card_draw = self.createCard(self.game.draw_card[2].img_source, 350, 200)
        self.__pot = self.createLabel("Pot : 0€", 285, 300)

        self.__buttonBetSmall = self.createButton("Bet 1", 520, 450)
        self.__buttonBetSmall['command'] = self.buttonBetSmall_click

        self.__buttonBetBig = self.createButton("Bet 2", 520, 420)
        self.__buttonBetBig['command'] = self.buttonBetBig_click

        self.__buttonFold = self.createButton("Fold", 520, 390)
        self.__buttonFold['command'] = self.buttonFold_click

        self.__dealer = self.initialise_dealer()

    def input_seed(self):
        """Fonction permettant d'entrer un seed et de générer le jeu avec le seed indiqué"""
        seed = askstring('SEED', 'Veuillez entrer un seed')
        if seed.isdigit() and seed != "":
            self.__init__(self.root, int(seed))
        else:
            messagebox.showinfo("Alerte","Le seed ne doit contenir que des chiffres entiers")

    def initialize_menu_bar(self):
        """Affiche la bare de menu dans la fenetre"""
        menubar = Menu(self.root)
        self.root.config(menu=menubar)
        menu_seed = Menu(menubar, tearoff=0)
        menubar.add_cascade(label="SEED", menu=menu_seed)
        menu_seed.add_command(label="Seed : " + str(self.game.seed))
        menu_seed.add_command(label="Ajouter un seed", command=self.input_seed)
        menu_replay = Menu(menubar, tearoff=0)
        menubar.add_cascade(label="REJOUER", menu=menu_replay)
        menu_replay.add_command(label="Relancer la partie", command=self.replay)
        menu_new_party = Menu(menubar, tearoff=0)
        menubar.add_cascade(label="NOUVELLE PARTIE", menu=menu_new_party)
        menu_new_party.add_command(label="Lancer une nouvelle partie", command=self.new_party)
        return menubar

    def replay(self):
        """Fonction permettant de rejouer la partie avec le même seed"""
        self.__init__(self.root, self.game.seed)

    def new_party(self):
        self.__init__(self.root, "")

    def initialise_dealer(self):
        """Affiche le jeton dealer au joueur qui est dealer"""
        if self.game.dealer == 0:
            return self.createDealer(150, 230)
        elif self.game.dealer == 1:
            return self.createDealer(305, 150)
        elif self.game.dealer == 2:
            self.createDealer(420, 230)
        else:
            self.createDealer(305, 350)


    def createDealer(self, axeX, axeY):
        """Permet de créer l'image du jeton dealer en fonction des axes indiqués en paramètre"""
        image = Image.open('ui/dealer.png')
        # Resize the image using resize() method
        resize_image = image.resize((20, 20))
        img = ImageTk.PhotoImage(resize_image)
        # create label and add resize image
        dealer = Label(image=img)
        dealer.image = img
        dealer.place(x=axeX, y=axeY)
        return dealer


    def buttonBetSmall_click(self):
        """Fonction exécutée lors de l'appui du bouton petite mise qui exécute la fonction bet_small_blind de la classe game"""
        self.game.bet_small_blind(self.game.list_player[3])
        self.__player_one_amount_label["text"] = str(self.game.list_player[3].amount) + "€"
        self.__pot["text"] = "Pot : " + str(self.game.pot) + " €"
        if self.game.list_player[3].amount == 0:
            self.__buttonBetSmall["state"] = DISABLED
            self.__buttonBetBig["state"] = DISABLED
        self.next_player(1)

    def buttonBetBig_click(self):
        """Fonction exécutée lors de l'appui du bouton grosse mise qui exécute la fonction bet_big_blind de la classe game"""
        self.game.bet_big_blind(self.game.list_player[3])
        self.__player_one_amount_label["text"] = str(self.game.list_player[3].amount) + "€"
        self.__pot["text"] = "Pot : "+str(self.game.pot)+" €"
        if self.game.list_player[3].amount == 0:
            self.__buttonBetSmall["state"] = DISABLED
            self.__buttonBetBig["state"] = DISABLED
        self.next_player(2)


    def buttonFold_click(self):
        """Fonction exécutée lors de l'appui du bouton fold qui rend non cliquable tous les boutons"""
        self.__buttonBetSmall["state"] = DISABLED
        self.__buttonBetBig["state"] = DISABLED
        self.__buttonFold["state"] = DISABLED
        self.end_of_round()

    def createLabel(self,text,axeX, axeY) -> tk.Label:
        """Affiche un label dans la fenetre du jeu en fonction du text et des axes x et y"""
        label = tk.Label(self.root)
        ft = tkFont.Font(family='Times',font="bold", size=8)
        label["font"] = ft
        label["fg"] = "#333333"
        label["justify"] = "center"
        label["text"] = text
        label.place(x=axeX, y=axeY, width=90, height=25)
        return label

    def createCard(self, img_source, axeX, axeY) -> tk.Label:
        """Affiche une image dans la fenetre du jeu en fonction de la source de l'image et des axes x et y"""
        image = Image.open('ui/' + img_source)
        # Resize the image using resize() method
        resize_image = image.resize((60, 75))
        img = ImageTk.PhotoImage(resize_image)
        # create label and add resize image
        hand_card = Label(image=img)
        hand_card.image = img
        hand_card.place(x=axeX, y=axeY)
        return hand_card

    def createButton(self, text, axeX, axeY) -> tk.Button:
        """Affiche un bouton dans la fenetre du jeu en fonction du text et des axes x et y"""
        button = tk.Button(self.root)
        button["bg"] = "#efefef"
        ft = tkFont.Font(family='Times', size=10)
        button["font"] = ft
        button["fg"] = "#000000"
        button["justify"] = "center"
        button["text"] = text
        button.place(x=axeX, y=axeY, width=70, height=25)
        return button

    def next_player(self, amount):
        """Initialise le pot général"""
        for ia in range(len(self.game.list_player) - 1):
            if amount == 1:
                self.game.bet_small_blind(self.game.list_player[ia])
            else:
                self.game.bet_big_blind(self.game.list_player[ia])
            self.__list_amount_bot[ia]["text"] = str(self.game.list_player[ia].amount) + " €"
            self.__pot["text"] = "Pot : " + str(self.game.pot) + " €"
        result = False
        for player in self.game.list_player:
            if player.amount > 0:
                result = True
        if result == False:
            self.end_of_round()

    def end_of_round(self):
        """Fonction permettant de gérer la fin du jeu"""
        pot_win = self.game.pot
        winner = self.game.check_winner()
        hand = self.game.check_value_win()
        self.print_all_card()
        name_and_win = ""
        if max(hand) < 14:
            name_and_win = "hauteur"
        elif max(hand) >= 14 and max(hand) < 28:
            name_and_win = "paire"
        elif max(hand) >= 28 and max(hand) < 42:
            name_and_win = "double paire"
        elif max(hand) >= 42 and max(hand) < 56:
            name_and_win = "un brelan"
        elif max(hand) >= 56 and max(hand) < 70:
            name_and_win = "une quinte"
        elif max(hand) >= 70 and max(hand) < 84:
            name_and_win = "une couleur"
        elif max(hand) >= 84 and max(hand) < 98:
            name_and_win = "un full house"
        elif max(hand) >= 98 and max(hand) < 112:
            name_and_win = "une quinte flush"
        elif max(hand) == 166:
            name_and_win = "une quinte flush royale"
        messagebox.showinfo("Fin de la manche", winner.name + " remporte le pot de " + str(pot_win) + " € avec " + name_and_win)
        result = False
        for player in self.game.list_player:
            if player.amount == 0:
                result = True
        if result:
            self.end_of_game(winner)
        else:
            self.game.next_round()
            self.initialize_next_round()

    def end_of_game(self, player):
        messagebox.showinfo("Fin du jeu", "Le joueur " + player.name + " gagne la partie avec un pot de " + str(player.amount) + " €")
        launch_new_party = tk.messagebox.askquestion('Rejouer ?', 'Voulez-vous lancer une nouvelle partie ?',
                                           icon='warning')
        if launch_new_party == "yes":
            self.__init__(self.root, "")
        else:
            self.__buttonBetSmall["state"] = DISABLED
            self.__buttonBetBig["state"] = DISABLED
            self.__buttonFold["state"] = DISABLED

    def print_all_card(self):
        """Affiche toutes les cartes des bots"""
        self.__hand_card_one_bot_one = self.createCard(self.game.list_player[0].hand[0].img_source, 0, 200)
        self.__hand_card_two_bot_one = self.createCard(self.game.list_player[0].hand[1].img_source, 70, 200)
        self.__hand_card_one_bot_two = self.createCard(self.game.list_player[1].hand[0].img_source, 250, 40)
        self.__hand_card_two_bot_two = self.createCard(self.game.list_player[1].hand[1].img_source, 320, 40)
        self.__hand_card_one_bot_three = self.createCard(self.game.list_player[2].hand[0].img_source, 460, 200)
        self.__hand_card_two_bot_three = self.createCard(self.game.list_player[2].hand[1].img_source, 530, 200)

    def initialize_next_round(self):
        """Initialise le prochain round avec les nouvelles cartes des ordinateurs"""
        self.list_amount_bot = []
        self.hand_card_one_bot_one = self.createCard("cartes.jpg", 0, 200)
        self.hand_card_two_bot_one = self.createCard("cartes.jpg", 70, 200)
        self.bot_one_amount_label['text'] = str(self.game.list_player[0].amount) + " €"
        self.list_amount_bot.append(self.__bot_one_amount_label)

        self.bot_two_label = self.createLabel("Bot 2", 285, 10)
        self.hand_card_one_bot_two = self.createCard("cartes.jpg", 250, 40)
        self.hand_card_two_bot_two = self.createCard("cartes.jpg", 320, 40)
        self.bot_two_amount_label['text'] = str(self.game.list_player[1].amount) + " €"
        self.list_amount_bot.append(self.__bot_two_amount_label)

        self.bot_three_label = self.createLabel("Bot 3", 495, 170)
        self.hand_card_one_bot_three = self.createCard("cartes.jpg", 460, 200)
        self.hand_card_two_bot_three = self.createCard("cartes.jpg", 530, 200)
        self.bot_three_amount_label['text'] = str(self.game.list_player[2].amount) + " €"
        self.list_amount_bot.append(self.__bot_three_amount_label)

        self.player_one_label = self.createLabel("Joueur 1", 285, 370)
        self.hand_card_one_player_one = self.createCard(self.game.list_player[3].hand[0].img_source, 250, 400)
        self.hand_card_two_player_one = self.createCard(self.game.list_player[3].hand[1].img_source, 320, 400)
        self.player_one_amount_label['text'] = str(self.game.list_player[3].amount) + " €"

        self.first_card_draw = self.createCard(self.game.draw_card[0].img_source, 210, 200)
        self.two_card_draw = self.createCard(self.game.draw_card[1].img_source, 280, 200)
        self.three_card_draw = self.createCard(self.game.draw_card[2].img_source, 350, 200)
        self.pot = self.createLabel("Pot : 0€", 285, 300)

        self.__buttonBetSmall["state"] = ACTIVE
        self.__buttonBetBig["state"] = ACTIVE
        self.__buttonFold["state"] = ACTIVE


    def get_card_name(self, value) -> str:
        """Retourne le nom de la carte en fonction de la valeur passée en paramètre"""
        if value == 1:
            return "Deux"
        elif value == 2:
            return "Trois"
        elif value == 3:
            return "Quatre"
        elif value == 4:
            return "Cinq"
        elif value == 5:
            return "Six"
        elif value == 6:
            return "Sept"
        elif value == 7:
            return "Huit"
        elif value == 8:
            return "Neuf"
        elif value == 9:
            return "Dix"
        elif value == 10:
            return "Valet"
        elif value == 11:
            return "Dame"
        elif value == 12:
            return "Roi"
        elif value == 13:
            return "As"


    @property
    def game(self):
        return self.__game

    @game.setter
    def game(self, game):
        self.__game = game

    @property
    def list_amount_bot(self):
        return self.__list_amount_bot

    @list_amount_bot.setter
    def list_amount_bot(self, list_amount_bot):
        self.__list_amount_bot = list_amount_bot

    @property
    def bot_one_label(self):
        return self.__bot_one_label

    @bot_one_label.setter
    def bot_one_label(self, bot_one_label):
        self.__bot_one_label = bot_one_label

    @property
    def hand_card_one_bot_one(self):
        return self.__hand_card_one_bot_one

    @hand_card_one_bot_one.setter
    def hand_card_one_bot_one(self, hand_card_one_bot_one):
        self.__hand_card_one_bot_one = hand_card_one_bot_one

    @property
    def hand_card_two_bot_one(self):
        return self.__hand_card_two_bot_one

    @hand_card_two_bot_one.setter
    def hand_card_two_bot_one(self, hand_card_two_bot_one):
        self.__hand_card_two_bot_one = hand_card_two_bot_one

    @property
    def bot_one_amount_label(self):
        return self.__bot_one_amount_label

    @bot_one_amount_label.setter
    def bot_one_amount_label(self, bot_one_amount_label):
        self.__bot_one_amount_label = bot_one_amount_label

    @property
    def bot_two_label(self):
        return self.__bot_two_label

    @bot_two_label.setter
    def bot_two_label(self, bot_two_label):
        self.__bot_two_label = bot_two_label

    @property
    def hand_card_one_bot_two(self):
        return self.__hand_card_one_bot_two

    @hand_card_one_bot_two.setter
    def hand_card_one_bot_two(self, hand_card_one_bot_two):
        self.__hand_card_one_bot_two = hand_card_one_bot_two

    @property
    def hand_card_two_bot_two(self):
        return self.__hand_card_two_bot_two

    @hand_card_two_bot_two.setter
    def hand_card_two_bot_two(self, hand_card_two_bot_two):
        self.__hand_card_two_bot_two = hand_card_two_bot_two

    @property
    def bot_two_amount_label(self):
        return self.__bot_two_amount_label

    @bot_two_amount_label.setter
    def bot_two_amount_label(self, bot_two_amount_label):
        self.__bot_two_amount_label = bot_two_amount_label

    @property
    def bot_three_label(self):
        return self.__bot_three_label

    @bot_three_label.setter
    def bot_three_label(self, bot_three_label):
        self.__bot_three_label = bot_three_label

    @property
    def hand_card_one_bot_three(self):
        return self.__hand_card_one_bot_three

    @hand_card_one_bot_three.setter
    def hand_card_one_bot_three(self, hand_card_one_bot_three):
        self.__hand_card_one_bot_three = hand_card_one_bot_three

    @property
    def hand_card_two_bot_three(self):
        return self.__hand_card_two_bot_three

    @hand_card_two_bot_three.setter
    def hand_card_two_bot_three(self, hand_card_two_bot_three):
        self.__hand_card_two_bot_three = hand_card_two_bot_three

    @property
    def bot_three_amount_label(self):
        return self.__bot_three_amount_label

    @bot_three_amount_label.setter
    def bot_three_amount_label(self, bot_three_amount_label):
        self.__bot_three_amount_label = bot_three_amount_label

    @property
    def bot_two_amount_label(self):
        return self.__bot_two_amount_label

    @bot_two_amount_label.setter
    def bot_two_amount_label(self, bot_two_amount_label):
        self.__bot_two_amount_label = bot_two_amount_label

    @property
    def player_one_label(self):
        return self.__player_one_label

    @player_one_label.setter
    def player_one_label(self, player_one_label):
        self.__player_one_label = player_one_label

    @property
    def hand_card_one_player_one(self):
        return self.__hand_card_one_player_one

    @hand_card_one_player_one.setter
    def hand_card_one_player_one(self, hand_card_one_player_one):
        self.__hand_card_one_player_one = hand_card_one_player_one

    @property
    def hand_card_two_player_one(self):
        return self.__hand_card_two_player_one

    @hand_card_two_player_one.setter
    def hand_card_two_player_one(self, hand_card_two_player_one):
        self.__hand_card_two_player_one = hand_card_two_player_one

    @property
    def player_one_amount_label(self):
        return self.__player_one_amount_label

    @player_one_amount_label.setter
    def player_one_amount_label(self, player_one_amount_label):
        self.__player_one_amount_label = player_one_amount_label

    @property
    def first_card_draw(self):
        return self.__first_card_draw

    @first_card_draw.setter
    def first_card_draw(self, first_card_draw):
        self.__first_card_draw = first_card_draw

    @property
    def two_card_draw(self):
        return self.__two_card_draw

    @two_card_draw.setter
    def two_card_draw(self, two_card_draw):
        self.__two_card_draw = two_card_draw

    @property
    def three_card_draw(self):
        return self.__three_card_draw

    @three_card_draw.setter
    def three_card_draw(self, three_card_draw):
        self.__three_card_draw = three_card_draw

    @property
    def pot(self):
        return self.__pot

    @pot.setter
    def pot(self, pot):
        self.__pot = pot

    @property
    def root(self):
        return self.__root

    @root.setter
    def root(self, root):
        self.__root = root

    @property
    def seed(self):
        return self.__seed

    @seed.setter
    def seed(self, seed):
        self.__seed = seed