import unittest

from model.deck import Deck
from model.game import Game

class GameTest(unittest.TestCase):
    #Verificate generation card with spécific  and random seed
    def test_generation_card_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 123456789, 100).draw_card[0].value, 2)

    def test_generation_card_with_two_random_seed(self):
        self.assertNotEqual(Game(3, 1, 1, "", 100).seed, Game(3, 1, 1, "", 100).seed)

    def test_generation_dealer_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 123456789, 100).dealer, 0)

    def test_generation_dealer_with_two_specific_seed(self):
        self.assertNotEqual(Game(3, 1, 1, 123456789, 100).dealer, Game(3, 1, 1, 123456788, 100).dealer)

    #Verificate hand with specific seed
    def test_hand_color_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 710710819, 100).is_color(Game(3, 1, 1, 710710819, 100).list_player[3].hand), 9)

    def test_hand_is_four_of_a_kind_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 98980925, 100).is_four_of_a_kind(Game(3, 1, 1, 98980925, 100).list_player[3].hand), 12)

    def test_hand_is_pair_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 313402397, 100).is_pair(Game(3, 1, 1, 313402397, 100).list_player[3].hand), 11)

    def test_hand_is_brelan_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 820065644, 100).is_brelan(Game(3, 1, 1, 820065644, 100).list_player[3].hand), 7)

    def test_hand_is_straight_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 308847577, 100).is_straight(Game(3, 1, 1, 308847577, 100).list_player[3].hand), 12)

    def test_hand_is_straight_flush_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 490123259, 100).is_straight_flush(Game(3, 1, 1, 490123259, 100).list_player[3].hand), 10)

    def test_hand_is_straight_flush_royale_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 236873158, 100).is_straight_flush_royale(Game(3, 1, 1, 236873158, 100).list_player[0].hand), 166)

    def test_hand_is_double_pair_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 924098297, 100).is_double_pair(Game(3, 1, 1, 924098297, 100).list_player[2].hand), 5)

    def test_hand_is_full_house_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 311065882, 100).is_full_house(Game(3, 1, 1, 311065882, 100).list_player[3].hand), 1)

    def test_player_win_first_round_is_dealer_with_specific_seed(self):
        self.assertEqual(Game(3, 1, 1, 208379790, 100).check_winner().is_dealer, True)

    def test_player_win_first_round_is_dealer_with_specific_seed(self):
        self.assertNotEqual(Game(3, 1, 1, 123456789, 100).check_winner().is_dealer, True)

    #Verificate the deck before shuffle
    def test_deck_length(self):
        self.assertEqual(Deck().length_deck(), 52)

    def test_deck_sorted_value(self):
        self.assertEqual(Deck().deck[0].value, 13)

    def test_deck_sorted_categorie_clubs(self):
        self.assertEqual(Deck().deck[0].categorie, "Trefle")

    def test_deck_sorted_hearts(self):
        self.assertEqual(Deck().deck[27].categorie, "Coeur")

    def test_deck_sorted_diamonds(self):
        self.assertEqual(Deck().deck[40].categorie, "Carreau")

    def test_deck_sorted_spides(self):
        self.assertEqual(Deck().deck[14].categorie, "Pique")

    def test_deck_draw_hand(self):
        self.assertEqual(Deck().get_number_cards_from_deck(2)[0].img_source, "dame_carreau.png")

if __name__ == "__main__":
    unittest.main()