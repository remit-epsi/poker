import tkinter as tk
from ui.app import App


if __name__ == "__main__":
    """Lancement du jeu"""
    root = tk.Tk()
    app = App(root, "")
    root.mainloop()


